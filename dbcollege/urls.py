from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^pages/', include('django.contrib.flatpages.urls')),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^events/', include('photologue.urls', namespace='photologue')),
    url(r'', include("website.urls", namespace="website")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
