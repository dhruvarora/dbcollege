from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^contact/$', views.contact, name="contact"),
    url(r'^departments/(?P<deptid>\d+)/(?P<deptname>.+)/$', views.departmentPage, name="department"),
    url(r'^departments/', views.departmentIndex, name="departments-index"),
    url(r'^library/$', views.libraryIndex, name="library-index"),
    url(r'^student-union/$', views.studentUnion, name="student-union"),
    url(r'^governing-body/$', views.governingBody, name="governing-body"),
    url(r'^admin-staff/$', views.adminStaff, name="admin-staff"),    
]
