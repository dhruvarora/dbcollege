# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.flatpages.admin import FlatPageAdmin
from django.contrib.flatpages.models import FlatPage
from django.db import models
from ckeditor_uploader.widgets import CKEditorUploadingWidget as CKEditorWidget
from .models import GoverningBodyMember, StudentUnionMember, StaffMember, Department, LibraryStaff, LibraryNewArrivals


class FlatPageCustom(FlatPageAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget}
    }


admin.site.unregister(FlatPage)
admin.site.register(FlatPage, FlatPageCustom)
admin.site.register(GoverningBodyMember)
admin.site.register(StudentUnionMember)
admin.site.register(StaffMember)
admin.site.register(Department)
admin.site.register(LibraryStaff)
admin.site.register(LibraryNewArrivals)
