# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from .models import GoverningBodyMember, StaffMember, StudentUnionMember, Department, LibraryNewArrivals, LibraryStaff

def index(request):
    gbmembers = GoverningBodyMember.objects.all()
    staffmembers = StaffMember.objects.all()

    return render(request, "index.html", {
    "gb":gbmembers, "staff":staffmembers,
    })

def contact(request):
    return render(request, "contact.html", {})

def departmentIndex(request):
    departments = Department.objects.all()

    return render(request, "departments.html", {
    "departments":departments,
    })

def departmentPage(request, deptid, deptname):
    department = Department.objects.get(id=deptid)

    return render(request, "department-page.html", {
    "dept":department,
    })

def libraryIndex(request):
    new_arrivals = LibraryNewArrivals.objects.all()
    staff = LibraryStaff.objects.all()

    return render(request, "library.html", {
    "newarrivals":new_arrivals, "staff":staff,
    })

def studentUnion(request):
    members = StudentUnionMember.objects.all()

    return render(request, "student-union.html", {
    "members":members,
    })

def adminStaff(request):
    members = StaffMember.objects.all()

    return render(request, "admin-staff.html", {
    "members":members,
    })

def governingBody(request):
    members = GoverningBodyMember.objects.all()

    return render(request, "governing-body.html", {
    "members":members,
    })
