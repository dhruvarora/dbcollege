# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from ckeditor_uploader.fields import RichTextUploadingField

class GoverningBodyMember(models.Model):
    date_added = models.DateTimeField(default=timezone.now)
    name = models.CharField(max_length=150)
    picture = models.FileField(max_length=150, upload_to="gb-members")
    title = models.CharField(max_length=150)

    def __str__(self):
        return self.name


class StaffMember(models.Model):
    date_added = models.DateTimeField(default=timezone.now)
    name = models.CharField(max_length=150)
    picture = models.FileField(max_length=150, upload_to="gb-members")
    title = models.CharField(max_length=150)

    def __str__(self):
        return self.name

class StudentUnionMember(models.Model):
    date_added = models.DateTimeField(default=timezone.now)
    name = models.CharField(max_length=150)
    picture = models.FileField(max_length=150, upload_to="gb-members")
    title = models.CharField(max_length=150)

    def __str__(self):
        return self.name

class Department(models.Model):
    date_added = models.DateTimeField(default=timezone.now)
    name = models.CharField(max_length=200)
    image = models.FileField(max_length=200, upload_to="departments")
    description = RichTextUploadingField(blank=True ,null=True)
    side1desc = RichTextUploadingField(blank=True ,null=True)
    side2desc = RichTextUploadingField(blank=True ,null=True)

    def __str__(self):
        return self.name

class LibraryStaff(models.Model):
    date_added = models.DateTimeField(default=timezone.now)
    name = models.CharField(max_length=200)
    image = models.FileField(max_length=200, upload_to="library/staff")
    title = models.CharField(max_length=150)

    def __str__(self):
        return self.name

class LibraryNewArrivals(models.Model):
    date_added = models.DateTimeField(default=timezone.now)
    name = models.CharField(max_length=200)
    image = models.FileField(max_length=200, upload_to="library/books")
    author = models.CharField(max_length=150)

    def __str__(self):
        return self.name
