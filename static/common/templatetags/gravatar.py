import hashlib
from urllib.parse import urlencode
from django import template
from django.utils.safestring import mark_safe


register = template.Library()


@register.filter
def gravatar_url(email, size=40):
    """
    Return only the url of the gravatar.
    Usage: {{ email|gravatar_url:200 }}
    """
    # default = static('img/default-profile.png')
    default = 'mm'

    email_hash = hashlib.md5(email.lower().encode('utf-8')).hexdigest()
    params = urlencode({'d': default, 's': str(size)})
    return "https://www.gravatar.com/avatar/%s?%s" % (email_hash, params)


@register.simple_tag
def gravatar(email, size=40, css=None):
    """
    Return an image tag with a gravatar
    Usage: {% gravatar email 30 'img-rounded' %}
    """
    url = gravatar_url(email, size)
    if css is None:
        tag = "<img src='{0}' height='{1}' width='{1}'>".format(url, size)
    else:
        tag = "<img src='{0}' height='{1}' width='{1}' class='{2}'>".format(url, size, css)
    return mark_safe(tag)
