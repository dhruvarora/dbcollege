from django import forms
from django.utils.datastructures import MultiValueDict
from django.utils.encoding import force_text
from django.utils.html import format_html
from django_select2.forms import Select2MultipleWidget


class ArrayFieldSelect2(Select2MultipleWidget):

    # def render_options(self, *args, **kwargs):
    #     try:
    #         selected_choices, = args
    #     except ValueError:  # Signature contained `choices` prior to Django 1.10
    #         choices, selected_choices = args
    #     output = ['<option></option>' if not self.is_required and not self.allow_multiple_selected else '']
    #     selected_choices = {force_text(v) for v in selected_choices.split(',')}
    #     choices = {(v, v) for v in selected_choices}
    #     for option_value, option_label in choices:
    #         output.append(self.render_option(selected_choices, option_value, option_label))
    #     return '\n'.join(output)

    def render_options(self, selected_choices):
        # Normalize to strings.
        selected_choices = set(force_text(v) for v in selected_choices.split(','))
        output = ['<option></option>' if not self.is_required and not self.allow_multiple_selected else '']
        for option_value, option_label in self.choices:
            if isinstance(option_label, (list, tuple)):
                output.append(format_html('<optgroup label="{}">', force_text(option_value)))
                for option in option_label:
                    output.append(self.render_option(selected_choices, *option))
                output.append('</optgroup>')
            else:
                output.append(self.render_option(selected_choices, option_value, option_label))
        return '\n'.join(output)

    def value_from_datadict(self, data, files, name):
        values = super().value_from_datadict(data, files, name)
        return ",".join(values)
