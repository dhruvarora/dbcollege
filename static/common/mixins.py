class SessionKeyMixin(object):

    @classmethod
    def get_session_key_prefix(cls):
        return getattr(cls, 'session_key_prefix', cls.__name__.lower())

    @classmethod
    def get_session_key_for_id(cls, id):
        return "{}:{}".format(cls.get_session_key_prefix(), id)

    @property
    def session_key(self):
        return "{}:{}".format(self.get_session_key_prefix(), self.id)
