from urllib.parse import urlencode
from django.urls import reverse as django_reverse


def reverse(viewname, kwargs=None, query_kwargs=None):
    """
    Custom reverse which adds a query string
    """
    url = django_reverse(viewname, kwargs=kwargs)
    if query_kwargs:
        return '{}?{}'.format(url, urlencode(query_kwargs))
    return url
