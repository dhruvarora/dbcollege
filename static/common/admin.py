from django.contrib.admin.filters import RelatedFieldListFilter, ChoicesFieldListFilter


class DropdownFilter(ChoicesFieldListFilter):
    template = 'admin/dropdown_filter.html'


class RelatedDropdownFilter(RelatedFieldListFilter):
    template = 'admin/dropdown_filter.html'
